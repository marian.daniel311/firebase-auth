import React , { useState } from 'react';
import { StyleSheet, Text, View, TouchableOpacity , Alert} from 'react-native';

import * as Facebook from 'expo-facebook';
import * as firebase from 'firebase';

const config={
    apiKey: "AIzaSyAys6dMgbqrs5ZL_GI5-2mrmToH-4eA1ts",
    authDomain: "fir-auth-dd7dd.firebaseapp.com",
    databaseURL: "https://fir-auth-dd7dd.firebaseio.com",
    projectId: "fir-auth-dd7dd",
    storageBucket: "fir-auth-dd7dd.appspot.com",
    messagingSenderId: "732456501871",
    appId: "1:732456501871:web:8af2b3d62577605e39c39a"
}

firebase.initializeApp(config);

export default function App() {

  const [logged, setLogged ] = useState(false);
  const [name, setName ] = useState('');

  async function signInWithFacebook() {
    try {
      await Facebook.initializeAsync('254688522568231');
      const {  type, token } = await Facebook.logInWithReadPermissionsAsync({
        permissions: ['public_profile'],
      });

      if (type === 'success') {
          const credential  = firebase.auth.FacebookAuthProvider.credential(token);
          firebase.auth().signInWithCredential(credential).then((response) => {
              setLogged(true);
              setName(response.user.displayName);
          }).catch((err) => {
              console.log(err);
          });
      } else {
          console.log('user pressed cancel')
      }
    } catch (err) {
      alert(`Facebook login err : ${err.message}`);
    }
  };

  function signOut() {
    firebase.auth().signOut().then(function() {
      setLogged(false);
    }).catch(function(error) {
        console.log(error);
    });
  }
  

  return (
    <View style={styles.container}>

      <Text>Greet & Code Job Interview Test</Text>
      <Text>Firebase Facebook Auth</Text>

      {logged ?
       <TouchableOpacity style={styles.fbLoginButton} onPress={() => signOut()}>
        <Text style={styles.loginButtonText}>Logout</Text>
    </TouchableOpacity>
      :
      <TouchableOpacity style={styles.fbLoginButton} onPress={() => signInWithFacebook()}>
          <Text style={styles.loginButtonText}>Sign in with Facebook</Text>
      </TouchableOpacity>
      }

      <Text>You are {logged ? 'logged in' : 'logged off'}</Text>

      {logged && 
      <Text style={{marginTop:20}}>User: {name}</Text>
      }

    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    paddingHorizontal:10,
    justifyContent: 'center',
  },
  fbLoginButton:{
    paddingVertical:15,
    paddingHorizontal:40,
    marginTop:40,
    backgroundColor:'#3B5998',
    borderRadius:10,
    flexDirection:'row',
    alignItems:'center',
    justifyContent:'center',
    marginBottom:30,
  },
  loginButtonText:{
    color:'#fff',
    fontSize:14,
    textAlign:'center',
  }
});
